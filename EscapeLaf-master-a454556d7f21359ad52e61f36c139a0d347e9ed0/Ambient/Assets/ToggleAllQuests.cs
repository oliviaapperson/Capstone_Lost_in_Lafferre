﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Devdog.QuestSystemPro;

public class ToggleAllQuests : MonoBehaviour {

    public Quest[] quests;

    private void Start()
    {
        foreach(Quest quest in quests)
        {
            string playerPrefsKey = QuestUtility.GetQuestCheckedSaveKey(quest);
            PlayerPrefs.SetInt(playerPrefsKey, 1);
        }
    }
}
