﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        

    }

    void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<Collider>().enabled = true;
        //collider.isTrigger = false;
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log("object is within the trigger");
        gameObject.GetComponent<Collider>().enabled = true;

    }

    void OnTriggerExit(Collider other)
    {

        gameObject.GetComponent<Collider>().enabled = false;
    }

}
