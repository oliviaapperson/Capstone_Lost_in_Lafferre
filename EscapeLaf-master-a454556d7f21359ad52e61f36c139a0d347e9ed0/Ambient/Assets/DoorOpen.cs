﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour {

    public Transform DoorOpenOpen;
    public float OpenAngleAmount = 88.0f;
    public float SmoothRotation;
    public string interactText = "Press J To Interact with Key";
    public GUIStyle InteractTextStyle;
    public bool hasEntered = false;

    private bool init = false;
    private bool doorOpen = false;
    private Vector3 startAngle;
    private Vector3 openAngle;
    private Rect interactTextRect;
    private AudioSource source;
    public GameObject Trigger;


    void Start()
    {
        keyItem.keyCount = 0;
        source = GetComponent<AudioSource>();
        //Check if Door Game Object is properly assigned
        if (DoorOpenOpen == null)
        {
            Debug.LogError(this + " :: Door Object Not Defined!");
        }

        //Init Start and Open door angles
        startAngle = DoorOpenOpen.eulerAngles;
        openAngle = new Vector3(startAngle.x, startAngle.y + OpenAngleAmount, startAngle.z);

        //Init Interact text Rect
        Vector2 textSize = InteractTextStyle.CalcSize(new GUIContent(interactText));
        interactTextRect = new Rect(Screen.width / 2 - textSize.x / 2, Screen.height - (textSize.y + 5), textSize.x, textSize.y);

        init = true;
    }

    void Update()
    {
        if (!init)
            return;

        HandleDoorRotation();
        HandleUserInput();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Object Entered the trigger");
        source.Play();
        doorOpen = doorOpen;
        gameObject.GetComponent<Collider>().enabled = true;
        //  keyItem.keyCount++;
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log("object is within the trigger");
        gameObject.GetComponent<Collider>().enabled = true;

    }

    void OnTriggerExit(Collider other)
    {
        if (keyItem.keyCount > 0)
        {
            keyItem.keyCount++;
            gameObject.GetComponent<Collider>().enabled = false;
            HandleDoorRotation();
            HandleUserInput();
        }
    }

    void OnGUI()
    {
        //if (!init || !hasEntered)
          //  return;

        GUI.Label(interactTextRect, interactText, InteractTextStyle);
    }

    void HandleDoorRotation()
    {
        if (doorOpen)
        {
            DoorOpenOpen.rotation = Quaternion.Slerp(DoorOpenOpen.rotation, Quaternion.Euler(startAngle), Time.deltaTime * SmoothRotation);
        }
        else
        {
            DoorOpenOpen.rotation = Quaternion.Slerp(DoorOpenOpen.rotation, Quaternion.Euler(openAngle), Time.deltaTime * SmoothRotation);
        }
    }

    void HandleUserInput()
    {
       // if (keyItem.keyCount > 0)
       // {
            source.Play();
            doorOpen = doorOpen;
       // }
    }
}

