﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewLevel : MonoBehaviour {
    public int currentLevel = 0;
    static NewLevel s = null;

    // Use this for initialization
    void Start () {
        if(s == null)
        {
            s = this;
        }
        else
        {
            Destroy(this.gameObject);

        }
        DontDestroyOnLoad(gameObject);
        
        
	}
	
	// Update is called once per frame
	public void Update () {
        if (currentLevel == 0)
        {
            if (Input.anyKeyDown)
            {
                currentLevel = 1;
                SteamVR_LoadLevel.Begin("iuhygfmnb");
            }
        }
	}
}
