﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Flashlight : MonoBehaviour {

    
    public Light myLight;
    public float life = 0;
    bool turnedOn = true;
    float _intensity;
    public float deltaTime = 0;
    int counter = 0;
    public float maxLife = 20;
    public float flickerTime = 10;

    private void Awake()
    {
        Debug.Log(life);
        //life = 10;
        _intensity = myLight.intensity;
        if (!turnedOn)
        {
            myLight.intensity = 0f;
        }

    }

    public void toggle()
    {
        turnedOn = !turnedOn;
        if (turnedOn)
        {
            myLight.intensity = _intensity;

        }
        else
        {
            myLight.intensity = 0f;
        }
    }

    public void Update()
    {
       
        if (turnedOn )
        {
            counter++;
            Debug.Log(life);
            life = Time.realtimeSinceStartup-deltaTime;
            Debug.Log(deltaTime + "Time left" + life);
             if(life < flickerTime)
            {
                myLight.intensity = _intensity;
            }
             else if (life >= maxLife)
             {
                myLight.intensity = 0f;
                SceneManager.LoadScene(0);
                turnedOn = !turnedOn;
             }
             else
             {
                if(counter % 10 == 0)
                {
                    myLight.intensity = Random.Range(0.5f, 1f);
                }
                else
                {
                    myLight.intensity = 0f;
                }
                /*float range = Random.Range(0f, 1.0f);
                    if (range > 0.3)
                    {
                        myLight.intensity = 0f;
                    }
                    else
                    {
                        myLight.intensity = 1f;
                    }*/
            }

        }
        else if(!turnedOn)
        {

            deltaTime = Time.realtimeSinceStartup-life;
            Debug.Log(deltaTime + "Off time");
           
        }
    }

   

}
