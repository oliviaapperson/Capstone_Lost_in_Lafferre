Thank you very much for your purchase and for your trust in LUDUS Assets. Your support helps us to continue creating high-quality content.
We hope you like this pack and if you are satisfied, we invite you to rate this product and leave a comment to continue improving.


OFFICE FURNITURE FULL PACK

The full pack with everything you need to build awesome office scenarios.
LUDUS Assets presents the most complete pack with modular and customizable assets and realistic 3D designs. Cupboards, lamps, keyboards, chairs� Discover all of the available assets and create high-quality office simulators.


-- WHAT DOES THE PACK INCLUDE? --

Demo Scene (Pictured).

Showroom Scene.

23 Low-Poly Prefabs for Office and Workplace Furniture (Ready for Desktop, Mobile and VR). All of them based on Autodesk Maya and Substance Pipeline.

16 HQ PBR Materials (Unity Standard, Metallic/Roughness Workflow). Albedo + Metallic/Roughness + Normal + Occlusion. PNG extension for better quality and uncompressed result.


--WHICH PREFABS WILL I FIND IN HERE? --

CardboardBox01 12 tris.
CardboardBox02 12 tris.
CardboardBox03 12 tris.
CardboardBox04 12 tris.
CardboardBox05 12 tris.
Cupboard 10 tris.
Cutter 40 tris.
Edding Red 88 tris.
Edding Black 88 tris.
Keyboard 56 tris.
Lamp 274 tris.
Locker 84 tris.
Office 286 tris.
OfficeCabinet01 96 tris.
OfficeCabinet02 204 tris.
OfficeChair 762 tris.
OfficeDesk 244 tris.
OpenTrash 114 tris.
PlasticBox01 10 tris.
PlasticBox02 10 tris.
PlasticBox03 10 tris.
Screen 111 tris.
 

-- WHAT IS LUDUS? --

We are a company specialized in the development of technology and VR simulators directed to the sectors of industry and emergency. Within our team we have designers specialized in 3D design that recreate scenarios and assets with great realism and precision. That is why we put at your disposal some of the assets we use in our own activity: high-quality elements and fully prepared for professional use.

We are at your disposal at assets@ludus-vr.com for any questions related to our products as well as support. We will be happy to receive your comments and suggestions to keep on improving!

Stay tuned visiting our website and joining our community on social networks:
http://www.ludus-vr.com

In case you are interested in our products, you can have a look at other related packs we offer here:
Modular Industrial Shelving Pack http://u3d.as/UQr 
Modular Warehouse Industrial Megapack http://u3d.as/UQ9 
Personal Protection Equipment http://u3d.as/UQx