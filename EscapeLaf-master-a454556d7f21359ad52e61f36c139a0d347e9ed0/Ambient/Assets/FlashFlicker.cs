﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashFlicker : MonoBehaviour
{
    public float fRunTimeFullCharge = 900.0f;
    public AnimationCurve ac = new AnimationCurve();

    float fTimer = 0.0f;
    float fMaxIntensity;
    public float chargeLevel; // Can't set level here
    public Light light;
    public void SetChargeLevel(float fCharge)
    {
        fTimer = (1.0f - Mathf.Clamp01(fCharge)) * fRunTimeFullCharge;
        Debug.Log(fTimer);
    }

    void Start()
    {
       
        fMaxIntensity = light.intensity;
        chargeLevel = 1.0f;
    }

    void Update()
    {
        if (fTimer < 0.0f || fTimer > fRunTimeFullCharge)
            return;
        chargeLevel = fTimer / fRunTimeFullCharge;
        light.intensity = ac.Evaluate(chargeLevel);
        fTimer += Time.deltaTime;

    }
}