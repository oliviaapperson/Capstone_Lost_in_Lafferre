﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DoorStuff
{
    public class DoorSlam : MonoBehaviour
    {
        // public string interactText = "Press J To Interact with Key";
        // public GUIStyle InteractTextStyle;
        public bool hasEntered = false;
        public Door doorHandler;

        void Start()
        {
            //Check if Door Game Object is properly assigned
            if (doorHandler == null)
            {
                Debug.LogError(this + " :: Door Object Not Defined!");
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "MainCamera" && hasEntered == false)
            {
                Debug.Log("Object Entered the trigger");
                doorHandler.CloseDoor();
                hasEntered = true;
            }
        }

        void OnTriggerStay(Collider other)
        {
            if (other.tag == "MainCamera" && hasEntered == false)
            {
                Debug.Log("object is within the trigger");
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag == "MainCamera" && hasEntered == false)
            {
                Debug.Log("object has left the trigger");
            }
        }
    }
}