﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoorStuff
{

    public class Door : MonoBehaviour
    {
        public Transform door;
        public float OpenAngleAmount = 88.0f;
        public float SmoothRotation;
        public bool doorOpenStartState = true;
        private bool doorOpenState = false;
        private Vector3 startAngle;
        private Vector3 openAngle;
        private AudioSource source;

        private void Start()
        {
            //Init Start and Open door angles
            startAngle = door.eulerAngles;
            openAngle = new Vector3(startAngle.x, startAngle.y + OpenAngleAmount, startAngle.z);
            source = GetComponent<AudioSource>();
            doorOpenState = doorOpenStartState;
        }

        // Use this for initialization
        void Update()
        {
            HandleDoorRotation();
        }

        public void OpenDoor()
        {
            doorOpenState = true;
            source.Play();
        }

        public void CloseDoor()
        {
            doorOpenState = false;
            source.Play();
        }

        // Update is called once per frame
        private void HandleDoorRotation()
        {
            if (!doorOpenState)
            {
                door.rotation = Quaternion.Slerp(door.rotation, Quaternion.Euler(startAngle), Time.deltaTime * SmoothRotation);
            }
            else
            {
                door.rotation = Quaternion.Slerp(door.rotation, Quaternion.Euler(openAngle), Time.deltaTime * SmoothRotation);
            }
        }
    }
}