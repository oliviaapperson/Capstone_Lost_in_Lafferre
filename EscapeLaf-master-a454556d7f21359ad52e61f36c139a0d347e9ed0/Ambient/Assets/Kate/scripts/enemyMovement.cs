﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class enemyMovement : MonoBehaviour
{

    public float wanderRadius;
    public float wanderTimer;

    private Transform target;
    public GameObject player;
    private NavMeshAgent agent;
    private float timer;

    public bool followPlayer;

    //public Collider other;
    // Use this for initialization
    void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        timer = wanderTimer;
        player = GameObject.FindGameObjectWithTag("MainCamera");

        followPlayer = false;
}

    

// Update is called once per frame
void Update()
{

    if (followPlayer == false)
    {
        timer += Time.deltaTime;

        if (timer >= wanderTimer)
        {
            Debug.Log("distance is" + wanderRadius);
            Debug.Log("distance is" + transform.position);
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            //if (target.collider == )
            //   {
            //GetComponent<NavMeshAgent>().destination = player.transform.position;
            //  }
            agent.SetDestination(newPos);
            timer = 0;
        }
    }
    else
    {
        agent.SetDestination(player.transform.position);


    }
}
     

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("ppooooooppy4");

        if (other.CompareTag("MainCamera") || other.CompareTag("player"))
        {
            followPlayer = true;
           
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCamera") || other.CompareTag("player"))
        {
            followPlayer = false;
        }
    }


    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {


        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }
}
