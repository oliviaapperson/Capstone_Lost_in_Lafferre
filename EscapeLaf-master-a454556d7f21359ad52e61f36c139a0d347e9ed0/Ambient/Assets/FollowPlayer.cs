﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour {

	// Use this for initialization
	

    private NavMeshAgent agent;
    public GameObject player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("MainCamera");
        if (!player)
        {
            Debug.Log("Make sure your player is tagged!!");
        }
    }
    void Update()
    {
        GetComponent<NavMeshAgent>().destination = player.transform.position;
    }
}
