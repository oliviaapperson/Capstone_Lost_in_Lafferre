﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class endGamescript : MonoBehaviour {

    public AudioSource scareSound;
    public int maxTime = 1000;
    private int minTime = 0;

    private float timer;
    private bool isEntered = false;
    private bool entertedAlready = false;
    private Light light;

    // Use this for initialization
    void Start()
    {
        light = GetComponent<Light>();

    }

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("MainCamera") && !entertedAlready)
        {
            timer = minTime;
            scareSound.Play();
            isEntered = true;
            entertedAlready = true;
            Debug.Log("worked");
            //wait(500);
            Application.LoadLevel(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("does it work???");
        timer = timer + 1;
        if (timer > maxTime)
        {
            light.enabled = true;
            if (isEntered)
            {
                light.intensity = .5f;
            }
            scareSound.Stop();

        }
        else if ((timer % 2 == 0) && isEntered)
        {
            light.intensity = Random.Range(2, 0);
            light.enabled = !light.enabled;
            Debug.Log(timer);
        }
    }
}
